ics-ans-role-nomachine
======================

Ansible role to install nomachine.

Requirements
------------

- ansible >= 2.3
- molecule >= 1.24

Role Variables
--------------

None

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-nomachine
```

License
-------

BSD 2-clause
